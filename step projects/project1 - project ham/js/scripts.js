"use strict";

//// GENERAL ////
function activateListButton(the_List, class_, additional_func) {
    for (let index = 0; index < the_List.length; index++) {
        let currentButton = the_List[index];
        let wholeAmount = 4;

        currentButton.addEventListener("click", function(){
            currentButton.classList.add(class_ + "_active");

            switch (additional_func) {
                case 1:
                    service_img.src = servicesArray[index][0];
                    service_desc.textContent = servicesArray[index][1];
                    wholeAmount = 5;
                    break;
                case 2:
                    the_reviewer.style = 'transition-duration: 0.4s; opacity: 0;';

                    setTimeout(function() {
                        the_reviewer.style = 'transition-duration: 0.6s; opacity: 1;';
                        reviewer_img.src = reviewersArray[index][0];
                        reviewer_review.textContent = reviewersArray[index][3];
                        reviewer_job.textContent = reviewersArray[index][2];
                        reviewer_name.textContent = reviewersArray[index][1];
                    }, 200);

                    wholeAmount = 3;
                    reviewer_pressed = index;
                    break;
                default:
                    let ourworks_imglist = document.getElementsByClassName('our_amazing_works_images_item');
                    loadMoreBtn.style.display = "none";

                    for (let ii = 0; ii < ourworks_imglist.length; ii++) {
                        ourworks_imglist[ii].style.display = "block";

                        if (((ourworks_imglist[ii].className === "our_amazing_works_images_item layer1") && btnPressAmount < 1) 
                        || ((ourworks_imglist[ii].className === "our_amazing_works_images_item layer2") && (btnPressAmount < 2))) {
                            ourworks_imglist[ii].style.display = "none";
                        }
                    }

                    const filterArray = [
                        [0, 0],
                        [1, "graphic_design"],
                        [2, "web_design"],
                        [3, "landing_pages"],
                        [4, "wordpress"]
                    ];

                    let activeFilter = filterArray[index][1];

                    if (activeFilter === 0) {
                        ourworks_imglist[index].style.display = "block";
                        loadMoreBtn.style.display = "block";
                        break;
                    }

                    for (let i = 0; i < ourworks_imglist.length; i++) {
                        if (ourworks_imglist[i].id != activeFilter) {
                            ourworks_imglist[i].style.display = "none";
                        }
                    }

                    break;
            }

            for (let index_ = 0; index_ <= wholeAmount; index_++) {
                if (index_ !== index) {
                    the_List[index_].className = class_;
                }
            }
        });
    }
}

// 'OUR SERVICES' SECTION //
const 
servicesArray = [
    [
        "./images/wordpress/wordpress1.jpg", 
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ],
    [
        "./images/wordpress/wordpress2.jpg", 
        "Lorrem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ],
    [
        "./images/wordpress/wordpress3.jpg", 
        "Lorrrem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ],
    [
        "./images/wordpress/wordpress4.jpg", 
        "Lorrrrem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ],
    [
        "./images/wordpress/wordpress5.jpg", 
        "Lorrrrrem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ],
    [
        "./images/wordpress/wordpress6.jpg", 
        "Lorrrrrrem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    ]
],
section_list = document.getElementsByClassName('services_button'),
service_img = document.getElementById('service_img'),
service_desc = document.getElementById('service_desc');

activateListButton(section_list, "services_button", 1)

// 'OUR AMAZING WORKS' SECTION //
const ourworks_list = document.getElementsByClassName('our_amazing_works_button');

activateListButton(ourworks_list, "our_amazing_works_button", 0)

let btnPressAmount = 0;
const loadMoreBtn = document.getElementById("o_a_w_loadmore_id");
loadMoreBtn.addEventListener("click", function() {
    let oaw_list;

    if (btnPressAmount === 0) {
        oaw_list = document.getElementsByClassName("layer1");
        btnPressAmount++;
    } else {
        oaw_list = document.getElementsByClassName("layer2");
        btnPressAmount++;
        loadMoreBtn.remove();
    }

    for (let index = 0; index < oaw_list.length; index++) {
        oaw_list[index].style = "display: block;";
    }
});

// 'WHAT PEOPLE SAY ABOUT THEHAM' SECTION //
const 
reviewersArray = [
    [
        "./images/what_people_say/background.png", 
        "Hasan Ali", 
        "UX Designer", 
        "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis."
    ],
    [
        "./images/what_people_say/background (1).png", 
        "Ali Hasan", 
        "UI Designer", 
        "Integger dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis."
    ],
    [
        "./images/what_people_say/background (2).png", 
        "Asan Hali", 
        "Designer UX", 
        "Integgger dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis."
    ],
    [
        "./images/what_people_say/background (3).png", 
        "Hali Asan", 
        "Designer UI", 
        "Integggger dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.."
    ]
],
reviewers_list = document.getElementsByClassName('wps_list_item'),
reviewer_name = document.getElementById('wps_reviewer_name'),
reviewer_job = document.getElementById('wps_reviewer_job'),
reviewer_img = document.getElementById('wps_reviewer_img'),
reviewer_review = document.getElementById('wps_reviewer_review'),
reviewer_btn_right = document.getElementById('btn_right'),
reviewer_btn_left = document.getElementById('btn_left');

let
reviewer_pressed = 0;

function switchButton() {
    let currentButton = reviewers_list[reviewer_pressed];
    
    currentButton.classList.add("wps_list_item_active");

    the_reviewer.style = 'transition-duration: 0.4s; opacity: 0;';

    setTimeout(function() {
        the_reviewer.style = 'transition-duration: 0.6s; opacity: 1;';

        reviewer_img.src = reviewersArray[reviewer_pressed][0];
        reviewer_review.textContent = reviewersArray[reviewer_pressed][3];
        reviewer_job.textContent = reviewersArray[reviewer_pressed][2];
        reviewer_name.textContent = reviewersArray[reviewer_pressed][1];
    }, 200);

    for (let index_ = 0; index_ <= 3; index_++) {
        if (index_ !== reviewer_pressed) {
            reviewers_list[index_].className = "wps_list_item";
        }
    }
}

reviewer_btn_right.onclick = function(){
    reviewer_pressed += 1;

    if (reviewer_pressed > 3) {
        reviewer_pressed = 0;
    }

    switchButton()
};

reviewer_btn_left.onclick = function(){
    reviewer_pressed -= 1;

    if (reviewer_pressed < 0) {
        reviewer_pressed = 3;
    }

    switchButton()
};

activateListButton(reviewers_list, "wps_list_item", 2)

// 'GALLERY OF BEST IMAGES' SECTION //
const loadMoreBtn_ = document.getElementById("g_o_i_loadmore_id");
loadMoreBtn_.addEventListener("click", function() {
    let goi_list;

    goi_list = document.getElementsByClassName("layer3");
    
    for (let index = 0; index < goi_list.length; index++) {
        goi_list[index].style = "display: block;";
    }

    document.getElementById("goi_list_id").style.height = "1521px";
    loadMoreBtn_.remove();
});