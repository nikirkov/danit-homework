"use strict";

const [BTN_login, BTN_visit, dashboard, startup_text] = elements;

BTN_login.addEventListener('click', () => {
    const window = new Modal();

    const content = new interact_form();
    
    const input_email = new interact_input();
    const input_password = new interact_input();
    const input_login_btn = new interact_input();

    input_email.initAttribs('email', 'loginEmail', nil, nil, 'Обязательно');
    input_password.initAttribs('password', 'loginPassword', nil, nil, 'Обязательно');
    input_login_btn.initAttribs('submit', 'button', nil, 'Войти');

    window.create();
    
    window.insert(content.create());
    content.insert(input_email.create(), input_password.create(), input_login_btn.create());

    window.title('АВТОРИЗАЦИЯ', 'login-title');
    input_email.label('E-MAIL', 'label');
    input_password.label('ПАРОЛЬ', 'label');

    input_login_btn.event('click', () => {
        const authorization_process = () => {
            if (!input_is_correct(input_email) || !input_is_correct(input_password)) {
                input_email.error();
                input_password.error();
    
                return;
            }
    
            const data = {
                email: input_email.value,
                password: input_password.value,
            };
        
            if (data.email && data.password) {
                send_request(URL_login, 'POST', data)
                .then(response => {
                    if (response.status >= 200 && response.status <= 399) {
                        return response.text()
                    }
                    else {
                        throw new Error('Неверный email или пароль.')
                    }
                })
                .then(data => {
                    sessionStorage.setItem('token', data);

                    window.close();
                    
                    setup_filter();
                    toggle_button(BTN_login, BTN_visit);
                })
                .catch(error => {
                    alert(error.message)
                    console.error(error.message)
                });
            }
        }

        authorization_process();
    })
});

BTN_visit.addEventListener('click', () => {
    const window = new Modal();

    const content = new interact_form();
    const select = new interact_select();
    const choose = new interact_input();

    window.create();
    window.insert(content.create());
    window.title('Выберите врача', 'createVisit-title');

    select.addOption('Стоматолог', 'dentist');
    select.addOption('Терапевт', 'therapist');
    select.addOption('Кардиолог', 'cardiologist');
    select.initAttribs('createVisitSelect');

    choose.initAttribs('submit', 'button', nil, 'Выбрать');

    content.insert(select.create(), choose.create());

    choose.event('click', () => {
        window.close();
        
        receive_card(select.value);
    });
});

function receive_card(value) {
    if (value === 'dentist') {
        const d_card = new VisitDentist();
        d_card.create();

        d_card.submit.event('click', () => {
            if (!input_is_correct(d_card.reason) || !input_is_correct(d_card.fullname)) {
                d_card.reason.error();
                d_card.fullname.error();
            
                return;
            }

            request_postcard(d_card.value);

            d_card.close();
        });
    } else if (value === 'therapist') {
        const t_card = new VisitTherapist();
        t_card.create();

        t_card.submit.event('click', () => {
            if (!input_is_correct(t_card.reason) || !input_is_correct(t_card.fullname) || !input_is_correct(t_card.age)) {
                t_card.reason.error();
                t_card.fullname.error();
                t_card.age.error();

                return;
            }

            request_postcard(t_card.value);
            
            t_card.close();
        });
    } else if (value === 'cardiologist') {
        const c_card = new VisitCardiologist();
        c_card.create();

        c_card.submit.event('click', () => {
            if (
            !input_is_correct(c_card.reason) || !input_is_correct(c_card.fullname) || 
            !input_is_correct(c_card.blood_pressure) || !input_is_correct(c_card.body_mass_index) || 
            !input_is_correct(c_card.cardio_vasc_diseases) || !input_is_correct(c_card.age)
            ) {
                c_card.reason.error();
                c_card.fullname.error();
                c_card.blood_pressure.error();
                c_card.body_mass_index.error();
                c_card.cardio_vasc_diseases.error();
                c_card.age.error();

                return;
            }

            request_postcard(c_card.value);
            
            c_card.close();
        });
    }
}

function request_postcard(data) {
    send_request(URL_cards, 'POST', data)
        .then(response => response.json())
        .then(data => {
            sessionStorage.setItem(data.id, JSON.stringify(data));
            display_card(data, data.id);
        })
        .catch(error => {
            console.error(error.message)
        });
}

function display_card(data, id) {
    const 
    card = document.createElement('div'),
    
    cardList = document.createElement('ul'),
    cardList_identificator = document.createElement('p'),
    
    btn_delete = document.createElement('a'),
    btn_regulate = document.createElement('a'),
    btn_edit = document.createElement('a');

    btn_edit.setAttribute('href', '#');
    btn_edit.textContent = pencil_symbol;
    btn_edit.classList.add('edit-card');

    btn_delete.setAttribute('href', '#');
    btn_delete.className = "card-delete";
    btn_delete.textContent = cross_symbol;

    btn_regulate.setAttribute('href', '#');
    btn_regulate.textContent = '> Показать больше';

    cardList.className = 'card__list card__list--before';
    cardList_identificator.textContent = `Карта №${id}`;

    card.classList.add('card', 'card--before');
    
    card.id = id;

    const item_array = [];
    for (let key in data) {
        if (key != 'id') {
            const cardItem = document.createElement('li');
            cardItem.classList.add('card-item', 'hidden');
            cardItem.textContent = `${key}: ${data[key]}`;

            cardList.classList.add('card__list', 'card__list--before');
            cardList.append(cardItem);

            item_array.push(cardItem);
        }
    }

    cardList.append(cardList_identificator, btn_regulate);
    card.append(btn_delete, cardList, btn_edit);
    
    if (startup_text) {
        startup_text.classList.add('hidden');
    }
    
    dashboard.append(card);

    btn_regulate.addEventListener('click', () => {
        item_array.forEach(e => {
            if (e.classList.contains('hidden')) {
                e.classList.remove('hidden');
                cardList_identificator.classList.add('hidden');
            }
            else {
                e.classList.add('hidden');
                cardList_identificator.classList.remove('hidden');
            }
        });

        btn_regulate.classList.toggle('less');

        if (btn_regulate.classList.contains('less')) {
            btn_regulate.textContent = '^ Скрыть';
        }
        else {
            btn_regulate.textContent = '> Показать больше';
        }

        card.classList.toggle('card--before');
        cardList.classList.toggle('card__list--before');
    })

    btn_delete.addEventListener('click', () => {
        manipulate_card.delete(id);
    });

    btn_edit.addEventListener('click', () => {
        manipulate_card.edit(id);
    });
}

function apply_edited_data(data, id) {
    send_request(URL_cards + `${id}`, 'PUT', data)
        .then(response => response.json())
        .then(data => {
            sessionStorage.setItem(data.id, JSON.stringify(data));
            
            display_card(data, data.id);

            const oldCard = document.getElementById(id);
            const newCard = dashboard.lastElementChild;
            oldCard.replaceWith(newCard);
            oldCard.remove();
        })
        .catch(error => {
            console.log(error.message)
        });
}

function setup_filter() {
    const search_field = document.querySelector('.search');
    const search_half = document.querySelector('.search__half');

    const input_search = new interact_input();
    input_search.initAttribs('text', 'search-input', nil, nil, 'Поиск по ФИО');

    const input_target = new interact_input();
    input_target.initAttribs('text', 'search-input_s', nil, nil, 'Поиск по цели');

    const input_desc = new interact_input();
    input_desc.initAttribs('text', 'search-input_s', nil, nil, 'Поиск по описанию');

    const select_priority = new interact_select();
    select_priority.addOption('Все', 'Все');
    select_priority.addOption('Обычная', 'Обычная');
    select_priority.addOption('Приоритетная', 'Приоритетная');
    select_priority.addOption('Неотложная', 'Неотложная');
    select_priority.initAttribs('priority');

    const select_status = new interact_select();
    select_status.addOption('Все', 'Все');
    select_status.addOption('Открыт (Open)', 'Обычная');
    select_status.addOption('Завершен (Done)', 'Приоритетная');
    select_status.initAttribs('status');

    const select_doc = new interact_select();
    select_doc.addOption('Все', 'Все');
    select_doc.addOption('Стоматолог', 'Стоматолог');
    select_doc.addOption('Терапевт', 'Терапевт');
    select_doc.addOption('Кардиолог', 'Кардиолог');
    select_doc.initAttribs('doc');
    
    const search_btn = new interact_input();
    search_btn.initAttribs('search_button', 'button', nil, 'Найти');
    search_btn.event("click", ()=> {
        send_request(URL_cards, 'GET')
        .then(data =>data.json())
        .then(cards => {
            const newCards = cards.filter((content) => {
                const isIncluded = (argument, value) => {
                    return content[argument].toString().includes(value)
                }

                const isIncluded_lowercased = (argument, value) => {
                    return content[argument].toLowerCase().includes(value.toLowerCase())
                }

                return (
                    (select_priority.value === content["Срочность"] || select_priority.value === 'Все') &&
                    (select_doc.value == content['Врач'] || select_doc.value === 'Все') &&
                    (isIncluded('Цель', input_target.value) || !input_target.value) &&
                    (isIncluded('Краткое описание', input_desc.value) || !input_desc.value) &&
                    (isIncluded_lowercased('ФИО', input_search.value) || !input_search.value)
                )
        })

        document.querySelectorAll('.card').forEach(e => {
            if (e.classList.contains('selected')) {
                e.classList.remove('selected');
            }

            newCards.forEach((item) => {
                if (e.id == item.id) {
                    e.classList.add('selected');
                }
            })
        });
    })
        .catch(error => console.error(error.message));
    });

    search_half.append(input_search.create(), input_target.create(), input_desc.create(), select_doc.create(), select_priority.create(), select_status.create());
    search_field.append(search_btn.create());
}