// DOCTOR CLASSES

class Visit {
    constructor() {
        this.modal = new Modal();
        this.visitForm = new interact_form();

        this.modal.title('Заполните поля', 'visitModal-title');

        this.reason = new interact_input();
        this.reason.initAttribs('text', 'reasonField', nil, nil, 'Обязательно');

        this.desc = new interact_text();
        this.desc.initAttribs('introField', nil, nil, 'Обязательно');

        this.priority = new interact_select();
        this.priority.addOption('Обычная', 'Обычная');
        this.priority.addOption('Приоритетная', 'Приоритетная');
        this.priority.addOption('Неотложная', 'Неотложная');

        this.fullname = new interact_input();
        this.fullname.initAttribs('text', 'fullNameField', nil, nil, 'Обязательно');

        this.submit = new interact_input();
        this.submit.initAttribs('submit', 'button', nil, 'Создать');
    }
}

class VisitDentist extends Visit {
    constructor() {
        super();
    }

    create() {
        this.modal.create();
        this.modal.insert(this.visitForm.create());
        this.visitForm.insert(
            this.reason.create(),
            this.desc.create(),
            this.priority.create(),
            this.fullname.create(),
            this.submit.create(),
        );

        this.reason.label('Цель визита', 'label-dentist');
        this.desc.label('Краткое описание', 'label-dentist');
        this.priority.label('Срочность', 'label-dentist');
        this.fullname.label('ФИО', 'label-dentist');

        return this.modal;
    }

    event(evt = nil, fn = {}) {
        this.submit.event(evt, fn);
    }

    close() {
        this.modal.close()
    }

    setValues(newReason = text_empty, 
        newDesc = 'Отсутствует', 
        newEmergency = text_empty, 
        newFullName = text_empty) 
        {
            this.reason.value = newReason;
            this.desc.value = newDesc;
            this.priority.value = newEmergency;
            this.fullname.value = newFullName;
            this.submit.value = text_savechanges;
    }

    get value() {
        return {
            'Врач': 'Стоматолог',
            'Цель': this.reason.value,
            'Краткое описание': this.desc.value,
            'Срочность': this.priority.value,
            'ФИО': this.fullname.value,
        };
    }
}

class VisitTherapist extends Visit {
    constructor() {
        super();

        this.age = new interact_input();
        this.age.initAttribs('text', 'ageField', nil, nil, 'Обязательно');
    }

    create() {
        this.modal.create();
        this.modal.insert(this.visitForm.create());
        this.visitForm.insert(
            this.reason.create(),
            this.desc.create(),
            this.priority.create(),
            this.fullname.create(),
            this.age.create(),
            this.submit.create(),
        );

        this.reason.label('Цель визита', 'label-therapist');
        this.desc.label('Краткое описание', 'label-therapist');
        this.priority.label('Срочность', 'label-therapist');
        this.fullname.label('ФИО пациента', 'label-therapist');
        this.age.label('Возраст пациента', 'label-therapist')

        return this.modal;
    }

    error() {
        if (!input_is_correct(this.reason) || !input_is_correct(this.fullname) || !input_is_correct(this.age)) {
            this.reason.error();
            this.fullname.error();
            this.age.error();

            return;
        }
    }

    event(evt = nil, fn = {}) {
        this.submit.event(evt, fn);
    }

    close() {
        this.modal.close();
    }

    setValues(newReason = text_empty, 
        newDesc = 'Отсутствует', 
        newEmergency = text_empty, 
        newFullName = text_empty, 
        newAge = text_empty) 
        {
            this.reason.value = newReason;
            this.desc.value = newDesc;
            this.priority.value = newEmergency;
            this.fullname.value = newFullName;
            this.age.value = newAge;
            this.submit.value = text_savechanges;
    }

    get value() {
        return {
            'Врач': 'Терапевт',
            'Цель': this.reason.value,
            'Краткое описание': this.desc.value,
            'Срочность': this.priority.value,
            'ФИО': this.fullname.value,
            'Возраст': this.age.value,
        };
    }
}

class VisitCardiologist extends Visit {
    constructor() {
        super();

        this.blood_pressure = new interact_input();
        this.body_mass_index = new interact_input();
        this.cardio_vasc_diseases = new interact_input();
        this.age = new interact_input();

        this.blood_pressure.initAttribs('text', 'pressureField', nil, nil, 'Обязательно');
        this.body_mass_index.initAttribs('text', 'BMIField', nil, nil, 'Обязательно');
        this.cardio_vasc_diseases.initAttribs('text', 'prevDiseasesField', nil, nil, 'Обязательно');
        this.age.initAttribs('text', 'ageField', nil, nil, 'Обязательно');
    }

    create() {
        this.modal.create();
        this.modal.insert(this.visitForm.create());
        this.visitForm.insert(
            this.reason.create(),
            this.desc.create(),
            this.priority.create(),
            this.fullname.create(),
            this.blood_pressure.create(),
            this.body_mass_index.create(),
            this.cardio_vasc_diseases.create(),
            this.age.create(),
            this.submit.create(),
        );

        this.reason.label('Цель визита', 'label-cardiologist');
        this.desc.label('Краткое описание', 'label-cardiologist');
        this.priority.label('Срочность', 'label-cardiologist');
        this.fullname.label('ФИО', 'label-cardiologist');
        this.blood_pressure.label('Обычное давление', 'label-cardiologist');
        this.body_mass_index.label('Индекс массы тела', 'label-cardiologist');
        this.cardio_vasc_diseases.label('Перенесенные болезни ССС', 'label-cardiologist');
        this.age.label('Возраст', 'label-cardiologist');

        return this.modal;
    }

    event(evt = nil, fn = {}) {
        this.submit.event(evt, fn);
    }

    close() {
        this.modal.close();
    }

    setValues(newReason = text_empty, 
        newDesc = 'Отсутствует', 
        newEmergency = text_empty, 
        newFullName = text_empty, 
        newPressure = text_empty, 
        newBMI = text_empty, 
        newPrevDiseases = text_empty, 
        newAge = text_empty) 
        {
            this.reason.value = newReason;
            this.desc.value = newDesc;
            this.priority.value = newEmergency;
            this.fullname.value = newFullName;
            this.blood_pressure.value = newPressure;
            this.body_mass_index.value = newBMI;
            this.cardio_vasc_diseases.value = newPrevDiseases;
            this.age.value = newAge;
            this.submit.value = text_savechanges;
    }

    get value() {
        return {
            'Врач': 'Кардиолог',
            'Цель': this.reason.value,
            'Краткое описание': this.desc.value,
            'Срочность': this.priority.value,
            'ФИО': this.fullname.value,
            'Обычное давление': this.blood_pressure.value,
            'ИМТ': this.body_mass_index.value,
            'Перенесенные болезни ССС': this.cardio_vasc_diseases.value,
            'Возраст': this.age.value,
        };
    }
}