// GLOBAL VALUES + FUNCs

const dev_authorization = false;

if (dev_authorization) {
    sessionStorage.clear();
}

const 
nil = '',
text_area_maxlength = 22, // иногда текст может выходить за рамки, поэтому я ввел ограничение по тексту
text_area_maxlength_input = 31, // если оно всё ещё выходит то можно сделать значение меньше
cross_symbol = String.fromCharCode(0x2716),
pencil_symbol = String.fromCharCode(0x270E),
text_empty = 'Пусто',
text_savechanges = 'Сохранить',
URL_cards = 'https://ajax.test-danit.com/api/v2/cards/',
URL_login = URL_cards + 'login',
elements = [
    document.getElementById('btn_login'),
    document.getElementById('btn_create_visit'),
    document.getElementById('visit_postcontainer'),
    document.querySelector('.visit__default-text')
];

const manipulate_card = {
    edit: (id) => {
        const doc = JSON.parse(sessionStorage.getItem(id)),
        doc_name = doc['Врач'];
        
        switch (doc_name) {
            case 'Стоматолог':
                const edit_dent = new VisitDentist();
        
                edit_dent.create();
                
                const {
                    'Врач': doctor,
                    'Цель': reason,
                    'Краткое описание' : intro,
                    'Срочность': emer,
                    'ФИО': fullname,
                } = doc;
                
                edit_dent.setValues(reason, intro, emer, fullname);
                edit_dent.event('click', () => {
                    if (!input_is_correct(edit_dent.reason) || !input_is_correct(edit_dent.fullname)) {
                        edit_dent.reason.error();
                        edit_dent.fullname.error();
                        
                        return;
                    }
        
                    edit_dent.close();
    
                    apply_edited_data(edit_dent.value, id);
                })
    
                break;
            case 'Терапевт':
                const edit_ther = new VisitTherapist();
        
                edit_ther.create();
                
                const {
                    'Врач': doctor_,
                    'Цель': reason_,
                    'Краткое описание' : intro_,
                    'Срочность': emer_,
                    'ФИО': fullname_,
                    'Возраст': age,
                } = doc;
                
                edit_ther.setValues(reason_, intro_, emer_, fullname_, age);
                edit_ther.event('click', () => {
                    if (!input_is_correct(edit_ther.reason) || !input_is_correct(edit_ther.fullname) || !input_is_correct(edit_ther.age)) {
                        edit_ther.reason.error();
                        edit_ther.fullname.error();
                        edit_ther.age.error();
        
                        return;
                    }
        
                    edit_ther.close();
    
                    apply_edited_data(edit_ther.value, id);
                })
    
                break;
            case 'Кардиолог':
                const edit_card = new VisitCardiologist();
        
                edit_card.create();
                
                const {
                    'Врач': doctor__,
                    'Цель': reason__,
                    'Краткое описание' : intro__,
                    'Срочность': emer__,
                    'ФИО': fullname__,
                    'Обычное давление': pressure,
                    'ИМТ': BMI,
                    'Перенесенныё болезни ССС': prevDiseases,
                    'Возраст': age_,
                } = doc;
                
                edit_card.setValues(reason__, intro__, emer__, fullname__, pressure, BMI, prevDiseases, age_);
                edit_card.event('click', () => {
                    if (
                        !input_is_correct(edit_card.reason) || !input_is_correct(edit_card.fullname) || 
                        !input_is_correct(edit_card.blood_pressure) || !input_is_correct(edit_card.body_mass_index) || 
                        !input_is_correct(edit_card.cardio_vasc_diseases) || !input_is_correct(edit_card.age)
                    ) {
                        edit_card.reason.error();
                        edit_card.fullname.error();
                        edit_card.blood_pressure.error();
                        edit_card.body_mass_index.error();
                        edit_card.cardio_vasc_diseases.error();
                        edit_card.age.error();
        
                        return;
                    }
        
                    edit_card.close();
    
                    apply_edited_data(edit_card.value, id);
                })
    
                break;
            default:
                break;
        }
    },
    delete: (id) => {
        document.getElementById(id).remove();
        sessionStorage.removeItem(id);
        
        if (dashboard.children.length === 1) {
            startup_text.classList.remove('hidden');
        }
    
        send_request(URL_cards + `${id}`, 'DELETE')
        .then(response => {
            response.text()
        })
        .catch(error => {
            console.error(error.message)
        });
    }
}

document.addEventListener('DOMContentLoaded', () => {
    if (get_token()) {
        setup_filter();
        toggle_button(BTN_login, BTN_visit);

        for (const key in sessionStorage) {
            if (!isNaN(Number(key))) {
                display_card(JSON.parse(sessionStorage.getItem(key)), key);
            }
        }
    }
});

function input_is_correct(input_text) {
    return input_text.value.trim();    
}

function get_token() {
    return sessionStorage.getItem('token');    
}

function toggle_button(btn_to_show, btn_to_hide) {
    const bth_cls = btn_to_hide.classList, bts_cls = btn_to_show.classList;
    
    if (bth_cls.contains('hidden')) {
        bth_cls.remove('hidden');
        bts_cls.add('hidden');
    }
}

function send_request
    (
        url = URL_login, 
        method = 'GET', 
        body
    ) 
    {
    return fetch(url, {
        method: method,
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${get_token()}`,
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(body)
    })
}