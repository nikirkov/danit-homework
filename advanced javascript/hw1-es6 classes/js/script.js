"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get name() {
    return this.name;
  }

  get age() {
    return this.age;
  }

  get salary() {
    return this.salary;
  }

  set name(string) {
    this.name = string;
  }

  set age(number) {
    this.age = number;
  }

  set salary(amount) {
    this.salary = amount;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, programming_languages) {
    super(name, age, salary);
    this.lang = programming_languages;
  }

  get salary() {
    return (this.salary * 3);
  }
}

let programmer_1 = new Programmer("арвпфыв", 18, 10000, "java, javascipt");
let programmer_2 = new Programmer("дщзльамз", 24, 5000, "javascipt");
let programmer_3 = new Programmer("фывмаывпя", 42, 50000, "java");

console.log(programmer_1);
console.log(programmer_2);
console.log(programmer_3);