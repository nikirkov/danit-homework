"use strict";

const db = document.body;
let films_global = [];
const Request_URL = "https://ajax.test-danit.com/api/swapi/films";

const div_root = document.createElement('div');
div_root.className = "root";
db.prepend(div_root);

function fetch_complete(url) {
    return fetch(url).then(response => response.json());
}

let loading = false;
function create_animation(parent) {
    const div_load = document.createElement('h2'); 
    // заголоволок h2 только чтобы сделать 
    // надпись выразительнее и не лезть в css (лень)
    
    div_load.className = "load";
    div_load.id = "load";
    parent.after(div_load);
    
    div_load.textContent = "Loading";
    setInterval(function() {
        div_load.textContent = div_load.textContent + ".";
    }, 500);

    loading = true;
}

const 
film_list = document.createElement('ul'),
start_hr = document.createElement('hr'),
film_list_title = document.createElement('h1');

film_list_title.textContent = "'Star Wars' Film Series";

div_root.prepend(film_list_title, start_hr, film_list);

// я не сильно понял что анимация должна прогружать в доп задании
// поэтому сделал как смог понять
create_animation(start_hr);

fetch_complete(Request_URL)
.then(films => {
    setTimeout(() => {
        for (const i in films) {
            const {characters, episodeId, name, openingCrawl} = films[i];

            const item_filmlist = document.createElement('li');
            film_list.append(item_filmlist);

            const item_filmlist_title = document.createElement('h2');
            item_filmlist_title.textContent = name;
            item_filmlist.append(item_filmlist_title);

            const item_filmlist_crawl = document.createElement('p');
            item_filmlist_crawl.textContent = openingCrawl;
            item_filmlist.append(item_filmlist_crawl);

            const item_filmlist_episodeId = document.createElement('p');
            item_filmlist_episodeId.textContent = "Episode ID: " + episodeId;
            item_filmlist.append(item_filmlist_episodeId);

            const item_filmlist_title_charlist = document.createElement('h3');
            item_filmlist_title_charlist.textContent = "Characters:";
            item_filmlist.append(item_filmlist_title_charlist);

            const item_filmlist_episodeCharacters = document.createElement('ul');
            item_filmlist.append(item_filmlist_episodeCharacters);

            for (let i_ = 0; i_ < characters.length; i_++) {
                fetch_complete(characters[i_])
                .then(char => {
                    const episodeCharacters_item = document.createElement('li');
                    episodeCharacters_item.textContent = char.name;
                    item_filmlist_episodeCharacters.append(episodeCharacters_item);
                })
            }

            if (i > 0) {
                const item_filmlist_hr = document.createElement('hr');
                item_filmlist.prepend(item_filmlist_hr);
            }
        }

        if (loading == true) {
            let div_load = document.getElementById('load');
            div_load.remove();

            loading = false;
        }
    }, 2000);
})
.catch(error => console.error(error));

const fin_hr = document.createElement('hr');
db.append(fin_hr);