"use strict";

const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const div_root = document.createElement('div');
div_root.className = "root";
document.body.prepend(div_root);

const ul = document.createElement('ul');
div_root.prepend(ul);

function obj_includes(obj, status) {
  return (!obj.includes(status))  
}

for (const k in books) {
  const {author, name, price} = books[k];
  
  let item = document.createElement('li');
  item.textContent = `Author: ${author} | Name: ${name} | Price: ${price}.`;

  try {
    const currentObj = Object.keys(books[k]);
    const error_text = 'property is not defined in ' + k + ' object';

    if (obj_includes(currentObj, "author")) {
      throw new Error('Author' + error_text);
    }
    else if (obj_includes(currentObj, "name")) {
      throw new Error('Name' + error_text);
    }
    else if (obj_includes(currentObj, "price")) {
      throw new Error('Price' + error_text);
    }
    else {
      ul.append(item);
    }
  } catch (error) {
    console.error(error);
  }
}