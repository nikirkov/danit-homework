"use strict";

const
RequestURL_get_ip = "https://api.ipify.org/?format=json",
RequestURL_get_info = "https://ip-api.com/json/",
SearchingFields = "?fields=continent,country,regionName,city,district";

// RequestURL_get_info + IP + SearchingFields = Result

async function getRandomIP() {  
    const response = await fetch(RequestURL_get_ip);
    const ip = await response.json();

    return ip;
}

async function getIPinfo(ip) {
    const full_link = RequestURL_get_info + ip + SearchingFields;

    const response = await fetch(full_link);
    const ip_info = response.json();

    return ip_info;
}

async function buttonPressed() {
    const {ip} = await getRandomIP();
    const [continent, country, regionName, city, district] = await getIPinfo(ip);

    ip_.textContent = '- IP: ' + ip + ' -';
    continent_.textContent = 'Континент: ' + continent;
    country_.textContent = 'Страна: ' + country;
    region_.textContent = 'Регион: ' + regionName;
    city_.textContent = 'Город: ' + city;
    district_.textContent = 'Район: ' + district;
}

const 
btn = document.getElementById("button"),
ip_ = document.getElementById("ip_text"),
continent_ = document.getElementById("continent_text"),
country_ = document.getElementById("country_text"),
region_ = document.getElementById("region_text"),
city_ = document.getElementById("city_text"),
district_ = document.getElementById("district_text");

btn.addEventListener("click", () => {
    buttonPressed();
});