"use strict";

// 1
let clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

clients1 = clients1.filter(function(client) {
  return !clients2.includes(client); 
});

const clients3 = [...clients1, ...clients2];
console.log(clients3); // result

// 2
const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17, 
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];

const filtered_array = characters.map(function(obj_contains) {
  const {name, lastName, age, ...else_} = obj_contains;

  return {name, lastName, age};
});
console.log(filtered_array); // result

// 3
const user1 = {
  name: "John",
  years: 30
};

const {name, years: age, isAdmin = false} = user1;
console.log(name, age, isAdmin); // result

// 4
const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

const fullProfile = Object.assign(satoshi2018, satoshi2019, satoshi2020);
console.log(fullProfile); // result

// 5
const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
};

const new_books = [...books, bookToAdd];
console.log(new_books); // result

// 6
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const infoToAdd = {
  age: 49,
  salary: 0
}

const employee_new = {
  ...employee, 
  ...infoToAdd
};

console.log(employee_new); // result

// 7
const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // result: 'value'
alert(showValue());  // result: 'showValue'
