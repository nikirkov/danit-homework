
import React, { Component } from 'react';
import "./App.css"

import Button from './components/button/button';
import Modal from './components/modal/modal';

class App extends Component {
  state = {
    setModalActive: false,
  }

  openModal = (id) => {
    this.setState({setModalActive: id})
  }

  closeModal = (e) => {
    this.setState({setModalActive: false})
  }

  render() {
    const {setModalActive} = this.state;
    const closemodal = this.closeModal;

    return (
      <div className="App">
        <Button  
          bg_color = "black"  
          text = "Open first modal"  
          onClick = {() => this.openModal('firstModal')} 
        />
        <Button 
          bg_color = "gray" 
          text = "Open second modal" 
          onClick = {() => this.openModal('secondModal')} 
        />
        <Modal 
          id = "firstModal"
          className = "firstModal"
          setHidden = {closemodal}
          setActive = {setModalActive} 
          text = {[
            'Once you delete this file, it won’t be possible to undo this action.', 
            'Are you sure you want to delete it?'
          ]} 
          header = 'Do you want to delete this file?' 
          actions = {[
            <Button 
              text = "Ok" 
              bg_color = "rgba(0, 0, 0, 0.1)" 
              onClick = {closemodal}
            />, 
            <Button 
              text = "Cancel" 
              bg_color = "rgba(0, 0, 0, 0.1)" 
              onClick = {closemodal}
            />
          ]}
          closeButton = {true}
        />     
        <Modal 
          id = "secondModal"
          className = "secondModal"
          setHidden = {closemodal}
          setActive = {setModalActive} 
          text = {[
            'аоаоаоаоаоаоаооаоаоаоаоаоаоаоаоаооаоаоаоаоаоаоао', 
            'уаааааааааааааааауауауа уауауауауау фыыыыпававппавпавпав'
          ]} 
          header = 'агугугугагагагагагаггигиг' 
          actions = {[
            <Button 
              text = "да" 
              bg_color = "rgba(0, 0, 0, 0.1)" 
              onClick = {closemodal}
            />, 
            <Button 
              text = "нет" 
              bg_color = "rgba(0, 0, 0, 0.1)" 
              onClick = {closemodal}
            />
          ]}
          closeButton = {false}
        />
      </div>
    );
  }
}

export default App;