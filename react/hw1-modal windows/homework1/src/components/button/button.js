
import React, { Component } from 'react';
import "./button.scss";

class Button extends Component {
    render() {
        const {bg_color, text, onClick, className} = this.props;

        return (
            <button 
                className = {`button 
                    ${className ? " button__" + className : ''}
                `} 
                onClick = {onClick} 
                style = {{
                    backgroundColor: bg_color
                }}
            >
                {text}
            </button>
        );
    }
}

export default Button;

