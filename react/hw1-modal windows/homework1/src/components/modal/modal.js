
import React, { Component } from 'react';
import './modal.scss';

class Modal extends Component {
    render() {
        const {
            id, 
            className,
            header, 
            closeButton, 
            text, 
            actions, 
            setActive, 
            setHidden
        } = this.props;

        return (
            <div id = {id} className = {setActive === id ? "modal active" : "modal"} onClick = {setHidden}>
                <div className = {`modal__content${className ? " modal__" + className : ''}`} onClick = {(e) => e.stopPropagation()}>
                    <div className = "modal__header">
                        <h1 className = "modal__header-title">
                            {header}
                        </h1>
                        {closeButton ? <a href="#" className="modal__close" onClick = {setHidden}>{String.fromCharCode(0x2715)}</a> : null}   
                    </div>
                    <div className = "modal__body">{
                        text.map((text_, index) => {
                            return <p id = {'body-text-str-' + index}>{text_}</p>
                        })
                    }</div>
                    <div className = "modal__buttons">
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;

