"use strict";

let inputNumber, found = false;

while (!inputNumber || inputNumber.trim() === "" || isNaN(+inputNumber) || (inputNumber % 1 !== 0)) {
    inputNumber = prompt("Введите целое число");
}

console.log("Number: " + inputNumber);

for (let loopNumber = 0; loopNumber <= inputNumber; loopNumber++) {
    if (loopNumber % 5 === 0) {
        console.log(loopNumber);
        
        if (loopNumber === 5) {
            found = true;
        }
    }
}

if (found === false) {
    console.log("Sorry, no numbers");
}