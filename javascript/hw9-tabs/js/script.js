"use strict";

const tabs_list = document.getElementsByClassName('tabs-title');
const paragraphs_list = document.getElementsByClassName('tabs-content')[0].children;

for (let index = 1; index < 5; index++) {
    document.getElementsByClassName('tabs-content')[0].children[index].style.display = "none";
}

function activateListButton() {
    for (let index = 0; index < tabs_list.length; index++) {
        let currentButton = tabs_list[index];

        currentButton.addEventListener("click", () => {
            currentButton.className = "tabs-title active";
            paragraphs_list[index].style.display = "block";

            for (let index_ = 0; index_ < 5; index_++) {
                if (index_ !== index) {
                    tabs_list[index_].className = "tabs-title";
                    paragraphs_list[index_].style.display = "none";
                }
            }
        });
    }
}

activateListButton();