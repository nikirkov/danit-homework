"use strict";

//  CONSTS
const 
background = document.getElementById("background"),
hovered_logo = [document.getElementById("logo_hvr"), document.getElementById("logo_id"), document.getElementById("title_milan")],
detail_cont = document.getElementsByClassName("details_container"),
all_links = [
    [
        document.getElementById("mail_link"), 
        document.getElementById("phone_link") 
    ],
    document.getElementsByClassName("item_title")
],
clr_theme_btn = document.getElementById("clr_th_btn");

//  LETS
let color_main, color_secondary, milan_logo,
storage_key = localStorage.getItem('color_theme_key') || 1,
background_url;

//  FUNCTIONS
function SetVars(clr, clr2, logo, url) {
    color_main = clr;
    color_secondary = clr2;
    milan_logo = logo;
    background_url = url;
}

function SetUpLinks() {
    for (let i = 0; i < all_links.length; i++) {
        for (let i__ = 0; i__ < all_links[i].length; i__++) {
            all_links[i][i__].addEventListener('mouseover',function(){
                all_links[i][i__].style.color = color_main || "#F26522";
            })
        
            all_links[i][i__].addEventListener('mouseleave',function(){
                all_links[i][i__].style.color = "#FFFFFF";
            })
        }
    }
    
    hovered_logo[1].addEventListener('mouseover', () => {
        hovered_logo[2].style.color = color_main || "#F26522";
    })
    
    hovered_logo[1].addEventListener('mouseleave', () => {
        hovered_logo[2].style.color = "#FFFFFF";
    })
}

function SetUpTheme(theme) {
    if (theme == 1) {
        SetVars("#F26522", "#F26522", "./images/logo_hovered.png", "url(./images/background.png)");
        clr_theme_btn.innerHTML = "Тема: &#127769";
    } else {
        SetVars("#ADD8E6", "#5499C7", "./images/logo_hovered_blue.png", "url(./images/background_blue.png)");
        clr_theme_btn.innerHTML = "Тема: &#9728";
    }

    storage_key = theme;
    localStorage.setItem('color_theme_key', theme);

    background.style.backgroundImage = background_url;
    hovered_logo[0].src = milan_logo;
    clr_theme_btn.style.backgroundColor = color_secondary;
    
    for (let i = 0; i < detail_cont.length; i++) {
        detail_cont[i].style.backgroundColor = color_secondary;
    }

    SetUpLinks();
}

//  MAIN
SetUpTheme(storage_key);

clr_theme_btn.addEventListener("click", () => {
    if (storage_key == 1) {
        SetUpTheme(2);
    } else {
        SetUpTheme(1);
    }
});