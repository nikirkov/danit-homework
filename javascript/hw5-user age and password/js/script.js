"use strict";

let objectData, newUser;
function createNewUser() {
    let name = prompt("Имя");
    let surname = prompt("Фамилия");
    let bd = prompt("День рождения", "00.00.0000");
    bd = new Date (bd.substring(6, 10), bd.substring(3, 5), bd.substring(0, 2));

    if ((name === "") || (surname === "") || (name == null) || (surname == null)) {
        createNewUser();
    }

    newUser = {
        firstName: name,
        lastName: surname,
        birthday: bd,
        
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        
        getAge() {
            let currentTime = new Date();
            
            return (currentTime.getFullYear() - this.birthday.getFullYear());
        },

        getPassword() {
            return (this.firstName[0] + this.lastName.toLowerCase() + this.birthday.getFullYear());
        }
    };

    return newUser;
}

createNewUser()
console.log(newUser.getPassword())
console.log(newUser.getAge())