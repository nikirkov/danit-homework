"use strict";

let number1, number2, mathfun;

function getData(defnumber1, defnumber2, mathoperation) {
    defnumber1 = defnumber1 || 0;
    defnumber2 = defnumber2 || 0;
    mathoperation = mathoperation || "";

    number1 = +prompt("Введите первое число", defnumber1);
    number2 = +prompt("Введите второе число", defnumber2);
    mathfun = prompt("Введите математическую операцию", mathoperation);

    if ((isNaN(number1)) || (isNaN(number2)) || 
    !((mathfun === "+") || (mathfun === "*") || (mathfun === "/") || (mathfun === "-"))) 
    {
        getData(number1, number2, mathfun);
    }
}

getData();

function math_multiply(n1, n2) {
    return n1 * n2;
}

function math_sum(n1, n2) {
    return n1 + n2;
}

function math_divide(n1, n2) {
    return n1 / n2;
}

function math_sub(n1, n2) {
    return n1 - n2;
}

let result;
function doMath(n_1, n_2, m_f) {
    switch (m_f) {
        case "*":
            result = math_multiply(n_1, n_2);
            break;
        case "+":
            result = math_sum(n_1, n_2);
            break;
        case "/":
            result = math_divide(n_1, n_2);
            break;
        case "-":
            result = math_sub(n_1, n_2);
            break;            
        default:
            break;
    }
}

doMath(number1, number2, mathfun);

alert("Результат: " + result);
console.log("Результат: " + result);