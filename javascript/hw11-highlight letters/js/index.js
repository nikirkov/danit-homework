"use strict";

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keypress', (buttonPressed) => {
    buttons.forEach(button => {
        if ((buttonPressed.key === button.textContent) || (buttonPressed.key.toUpperCase() === button.textContent)) {
            button.style.backgroundColor = '#0000ff';
        } else {
            button.style.backgroundColor = '#000000';
        }
    });
});
