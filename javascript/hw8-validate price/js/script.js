"use strict";

const db = document.body;

const label = document.createElement('label');
db.append(label);
label.textContent = 'Price:';

const input = document.createElement('input');
input.style = 'margin-left: 10px; outline: 2px solid gray;';

input.addEventListener('focus', function() {
    input.style.outline = '2px solid green';

    if (paragraph) {
        paragraph.remove();
    }
});

label.append(input);

let paragraph;

const div = document.createElement('div');
db.prepend(div);

input.addEventListener('blur', function() {
    if (input.value == '' || isNaN(input.value)) {
        input.value = '';

        input.style.outline = '2px solid gray';
        return;
    } else if (input.value < 0) {
        paragraph = document.createElement('p');
        paragraph.textContent = 'Please, enter the correct price';
        label.append(paragraph);

        input.style.outline = '2px solid red';
        return;
    } else {
        const span = document.createElement('span');
        span.textContent = `Текущая цена: ${input.value}`;
        div.append(span);

        const button = document.createElement('button');
        button.textContent = 'X';
        button.style = 'margin-left: 4px; margin-right: 4px; margin-bottom: 8px;'     
        button.addEventListener('click', () => {
            span.remove();
        });
        span.append(button);

        input.value = '';
        input.style.outline = '2px solid gray';
        return;
    }
});
