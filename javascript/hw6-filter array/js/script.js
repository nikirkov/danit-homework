"use strict";

function filterBy(objectFilter, typeFilter) {
    return objectFilter.filter(objectItem => ((typeof objectItem) !== typeFilter));

}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

/* или вот так:
let newArray;
function filterBy(objectFilter, typeFilter) {
    newArray = [];
    for (let i = 0; i < objectFilter.length; i++) {
        if ((typeof objectFilter[i]) !== typeFilter) {
            newArray.push(objectFilter[i])
        }
    }

    return newArray;
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
*/