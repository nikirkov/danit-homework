"use strict";

// consts
const
intervalTime = 3000,
btnStop = document.getElementById("button_stop"),
btnCont = document.getElementById("button_cont"),
imagesID = [
    document.getElementById('image_1'),
    document.getElementById('image_2'),
    document.getElementById('image_3'),
    document.getElementById('image_4')
];

// lets
let
interval,
opacityToSet,
currentPicture = 0;

// funcs
function fade(element) {
    let op = 1;  // initial opacity
    let timer = setInterval(function () {
        if (op <= 0.015){
            clearInterval(timer);
            element.style.visibility = 'hidden';
        }
        
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}

function unfade(element) {
    let op = 0.1;  // initial opacity
    element.style.visibility = 'visible';
    let timer = setInterval(function () {
        if (op >= 1){
            clearInterval(timer);
        }

        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op += op * 0.1;
    }, 10);
}

function createTimer() {
    deleteTimer();

    interval = setInterval(() => {
        for (let index = 0; index < imagesID.length; index++) {
            /* WITHOUT FADE
            (index == currentPicture) ? opacityToSet = 1 : opacityToSet = 0;

            imagesID[index].style.opacity = opacityToSet;
            */
            
            // WITH FADE
            (index == currentPicture) ? (unfade(imagesID[index])) : (fade(imagesID[index]));
        }

        (currentPicture < (imagesID.length - 1)) ? (currentPicture = currentPicture + 1) : (currentPicture = 0);
    }, intervalTime);
}

function deleteTimer() {
    if (interval) {
        clearInterval(interval);
    }
}

// main
createTimer();

// main - btns
btnStop.addEventListener("click", () => {
    deleteTimer();
});

btnCont.addEventListener("click", () => {
    createTimer();
});