"use strict";

$(document).ready(function () {
    const wind = $(window);
    wind.on('scroll', function() {
        (wind.scrollTop()*4 > wind.height()) ? $(".button_top").show('fast') : $(".button_top").hide('fast')
    });

    $('#button_searchtool').click(
        function() {
            ($('#burger_menu_id').css("display") == "none") ? (
                $('#search_tool_id').css('background-color', "#5B6469"),
                $('#burger_menu_id').css("display", "block")
            ) : (
                $('#search_tool_id').css('background-color', "#000000"),
                $('#burger_menu_id').css("display", "none")
            );
        }
    );
    
    $("#slide_toggle_id").click(
        function() {
            ($('.post_list').css("display") == "flex") ? $('.post_list').css("display", "none") : $('.post_list').css("display", "flex")
        }
    );

    $("a").on('click', 
        function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                    }, 1000, function() {
                    window.location.hash = hash;
                    }
                );
            }
        }
    );
});
