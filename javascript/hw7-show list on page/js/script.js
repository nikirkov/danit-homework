"use strict";

const db = document.body;
function addList(objectItems, parent) {
    let ul_list = document.createElement('ul');

    parent = parent || db;
    parent.appendChild(ul_list);

    for (let index = 0; index < objectItems.length; index++) {
        ul_list.insertAdjacentHTML('beforeend', objectItems.map((txtContent) => {
            return `<li>${txtContent}</li>`;
        })[index]);
    }
}

addList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);