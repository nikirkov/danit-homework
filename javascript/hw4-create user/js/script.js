"use strict";

// если я неправильно понял тз, то смогу переделать
let objectData, newUser;
function createNewUser() {
    let name = prompt("Имя");
    let surname = prompt("Фамилия");

    if ((name === "") || (surname === "") || (name == null) || (surname == null)) {
        createNewUser();
    }

    newUser = {
        firstName: name,
        lastName: surname,
        
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };

    return newUser;
}


console.log(createNewUser().getLogin())

/* OR
createNewUser();
console.log(newUser.getLogin())
*/