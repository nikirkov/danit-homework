"use strict";

const 
eyes = document.getElementsByClassName("fas"),
inputs = document.getElementsByTagName("input"),
def_class = "fas fa-eye icon-password", 
pressed_class = "fas fa-eye-slash icon-password",
confirm_btn = document.getElementById("btn_id");

let p = document.createElement("p");
p.textContent = 'Нужно ввести одинаковые значения';
p.style.color = 'red';

confirm_btn.addEventListener("click", () => {
    (inputs[0].value == inputs[1].value) ? (alert("You are welcome!")) : inputs[1].after(p);
});

for (let index = 0; index < 2; index++) {
    let 
    currentBtn = eyes[index],
    currentInput = inputs[index];
    currentBtn.addEventListener("click", () => {
        (currentBtn.className === def_class) ? (
            currentBtn.className = pressed_class,
            currentInput.type = "text"
        ) : (
            currentBtn.className = def_class,
            currentInput.type = "password"
        );
    });
}