const 
{ src, dest, watch, series, parallel } = require("gulp"),
sass = require("gulp-sass"),
browserSync = require("browser-sync").create(),
clean = require("gulp-clean"),
autoprefixer = require("gulp-autoprefixer"),
CleanCSS = require("gulp-clean-css"),
UglifyJS = require("gulp-uglify"),
imagemin = require("gulp-imagemin"),

style = () => {
  return src("./src/styles/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 25 versions"],
        cascade: false,
      })
    )
    .pipe(CleanCSS())
    .pipe(dest("./dist/css/"))
    .pipe(browserSync.stream());
},

scripts = () => {
  return src("./src/scripts/**/*.js").pipe(UglifyJS()).pipe(dest("./dist/js"));
},

watching = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  
  watch("./src/styles/**/*.scss", style);
  watch("./*.html").on("change", browserSync.reload);
  watch("./src/scripts/**/*js").on("change", browserSync.reload);
  watch("./src/scripts/**/*.js", scripts);
  watch("./src/images/**", imgCompressor);
},

cleaning = () => {
  return src("dist/*", { read: false }).pipe(clean());
},

imgCompressor = () => {
  return src("./src/images/**")
    .pipe(imagemin({ progressive: true }))
    .pipe(dest(`./dist/images/`));
};

exports.build = series(cleaning, parallel(style, scripts), imgCompressor);
exports.dev = series(
  cleaning,
  parallel(style, scripts),
  imgCompressor,
  watching
);
